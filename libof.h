/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef LIBOF_H
#define LIBOF_H

#include <stdint.h>
#include <time.h>
#include <sys/socket.h>

#include <hashtab.h>

#define OFP_VERSION_10		0x01

enum of_event_type {
	OFEV_CONNECTION_UP,
	OFEV_CONNECTION_DOWN,
	OFEV_PROTO_MESSAGE
};

struct of_event {
	enum of_event_type		 type;
	struct of_dataplane		*dp;
	union {
		struct ofp_header	*ofp_hdr;
	};
};

struct of_dataplane {
	int				 socket;
	int				 ready;
	struct sockaddr_storage		 sw_addr;
	time_t				 lastmsg_ts;
	const struct of_protocol	*protocol;
	struct ofp_header		*sw_features;
};

struct of_controller {
	int socket;
	int port;
	int ev_queue;
	int n_timers;
	struct hashtab conns;		/* map: int -> uintptr_t, socket to struct dataplane* */
	struct hashtab dpids;		/* map: uint64_t -> uinptr_t, dpid to struct dataplane* */
	struct hashtab timer_callbacks;	/* timer call backs */
	const struct of_protocol *protocol;
	void (*of_handler)(struct of_controller *, struct of_event *);

	void (*handler)(struct of_controller *, void (*)(struct of_controller *, struct of_event *));
	void (*timer)(struct of_controller *, unsigned int, void (*)(struct of_controller *));
	void (*send)(struct of_controller *, struct of_dataplane *, struct ofp_header *);
	void (*loop)(struct of_controller *);
};

struct of_protocol {
	uint8_t version;
	void (*init)(struct of_controller *);
	int (*handshake)(struct of_controller *, struct of_dataplane *);	/* async handshake */
	int (*recv)(struct of_controller *, struct of_dataplane *, struct ofp_header *);
	int (*ping)(struct of_controller *, struct of_dataplane *);	/* async ping */
};

/*
	assume that ofp_header will be the same across all OpenFlow versions
*/
struct ofp_header {
	uint8_t version;
	uint8_t type;
	uint16_t length;
	uint32_t xid;
}__attribute__((packed));
/* assert(sizeof(struct ofp_header) == 8); */


int of_controller_init(struct of_controller *, int, const struct of_protocol *);

#endif

