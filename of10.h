/*
 * Copyright (c) 2015 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef of10_H
#define of10_H

#include <stdint.h>
#include "libof.h"

#define OFPT10_HELLO			0
#define OFPT10_OFPT_ERROR		1
#define OFPT10_ECHO_REQUEST		2
#define OFPT10_ECHO_REPLY		3
#define OFPT10_VENDOR			4
#define OFPT10_FEATURES_REQUEST		5
#define OFPT10_FEATURES_REPLY		6
#define OFPT10_GET_CONFIG_REQUEST	7
#define OFPT10_GET_CONFIG_REPLY		8
#define OFPT10_SET_CONFIG		9
#define OFPT10_PACKET_IN		10
#define OFPT10_FLOW_REMOVED		11
#define OFPT10_PORT_STATUS		12
#define OFPT10_PACKET_OUT		13
#define OFPT10_FLOW_MOD			14
#define OFPT10_PORT_MOD			15
#define OFPT10_STATS_REQUEST		16
#define OFPT10_STATS_REPLY		17
#define OFPT10_BARRIER_REQUEST		18
#define OFPT10_BARRIER_REPLY		19
#define OFPT10_QUEUE_GET_CONFIG_REQUEST	20
#define OFPT10_QUEUE_GET_CONFIG_REPLY	21

#define OFP10_ETH_ALEN			6
#define OFP10_MAX_PORT_NAME_LEN		16

/* packet_in reason */
#define OFPR10_NO_MATCH			0
#define OFPR10_ACTION			1

/* action type */
#define OFPAT10_OUTPUT			0
#define OFPAT10_SET_VLAN_VID		1
#define OFPAT10_SET_VLAN_PCP		2
#define OFPAT10_STRIP_VLAN		3
#define OFPAT10_SET_DL_SRC		4
#define OFPAT10_SET_DL_DST		5
#define OFPAT10_SET_NW_SRC		6
#define OFPAT10_SET_NW_DST		7
#define OFPAT10_SET_NW_TOS		8
#define OFPAT10_SET_TP_SRC		9
#define OFPAT10_SET_TP_DST		10
#define OFPAT10_ENQUEUE			11
#define OFPAT10_VENDOR			0xffff

/* flow mod command */
#define OFPFC10_ADD			0
#define OFPFC10_MODIFY			1
#define OFPFC10_MODIFY_STRICT		2
#define OFPFC10_DELETE			3
#define OFPFC10_DELETE_STRICT		4

/* port */
#define OFPP10_IN_PORT			0xfff8
#define OFPP10_NORMAL			0xfffa
#define OFPP10_FLOOD			0xfffb
#define OFPP10_ALL			0xfffc
#define OFPP10_CONTROLLER		0xfffd
#define OFPP10_NONE			0xffff

/* flow wildcards */
#define OFPFW10_IN_PORT			1 << 0
#define OFPFW10_DL_VLAN			1 << 1
#define OFPFW10_DL_SRC			1 << 2
#define OFPFW10_DL_DST			1 << 3
#define OFPFW10_DL_TYPE			1 << 4
#define OFPFW10_NW_PROTO		1 << 5
#define OFPFW10_TP_SRC			1 << 6
#define OFPFW10_TP_DST			1 << 7
#define OFPFW10_ALL			((1 << 22) - 1)

/* flow mod flags */
#define OFPFF_SEND_FLOW_REM		1 << 0
#define OFPFF_CHECK_OVERLAP		1 << 1
#define OFPFF_EMERG			1 << 2

/* stats types */
#define OFPST10_DESC			0
#define OFPST10_FLOW			1
#define OFPST10_AGGREGATE		2
#define OFPST10_TABLE			3
#define OFPST10_PORT			4
#define OFPST10_QUEUE			5
#define OFPST10_VENDOR			0xffff


struct of10_phy_port
{
	uint16_t		port_no;
	uint8_t			hw_addr[OFP10_ETH_ALEN];
	char			name[OFP10_MAX_PORT_NAME_LEN];
	uint32_t		config;
	uint32_t		state;
	uint32_t		curr;
	uint32_t		advertised;
	uint32_t		supported;
	uint32_t		peer;
}__attribute__((packed));

struct of10_switch_features
{
	struct ofp_header	hdr;
	uint64_t		datapath_id;
	uint32_t		n_buffers;
	uint8_t			n_tables;
	uint8_t			pad[3];
	uint32_t		capabilities;
	uint32_t		actions;
	struct of10_phy_port	ports[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));

struct of10_packet_in
{
	struct ofp_header	hdr;
	uint32_t		buffer_id;
	uint16_t		total_len;
	uint16_t		in_port;
	uint8_t			reason;
	uint8_t			pad;
	uint8_t			data[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));

struct of10_action_header
{
	uint16_t		type;
	uint16_t		len;
	uint8_t			pad[4];
}__attribute__((packed));

struct of10_action_output
{
	uint16_t		type;
	uint16_t		len;
	uint16_t		port;
	uint16_t		max_len;
}__attribute__((packed));

struct of10_action_dl_addr
{
	uint16_t		type;
	uint16_t		len;
	uint8_t			dl_addr[OFP10_ETH_ALEN];
	uint8_t			pad[6];
}__attribute__((packed));

struct of10_action_nw_addr
{
	uint16_t		type;
	uint16_t		len;
	uint32_t		nw_addr;
}__attribute__((packed));

struct of10_packet_out
{
	struct ofp_header		hdr;
	uint32_t			buffer_id;
	uint16_t			in_port;
	uint16_t			actions_len;
	struct of10_action_header	actions[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));

struct of10_match {
	uint32_t		wildcards;
	uint16_t		in_port;
	uint8_t			dl_src[OFP10_ETH_ALEN];
	uint8_t			dl_dst[OFP10_ETH_ALEN];
	uint16_t		dl_vlan;
	uint8_t			dl_vlan_pcp;
	uint8_t			pad1[1];
	uint16_t		dl_type;
	uint8_t			nw_tos;
	uint8_t			nw_proto;
	uint8_t			pad2[2];
	uint32_t		nw_src;
	uint32_t		nw_dst;
	uint16_t		tp_src;
	uint16_t		tp_dst;
}__attribute__((packed));

struct of10_flow_mod
{
	struct ofp_header		hdr;
	struct of10_match		match;
	uint64_t			cookie;
	uint16_t			command;
	uint16_t			idle_timeout;
	uint16_t			hard_timeout;
	uint16_t			priority;
	uint32_t			buffer_id;
	uint16_t			out_port;
	uint16_t			flags;
	struct of10_action_header	actions[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));


struct of10_stats_request {
	struct ofp_header		hdr;
	uint16_t			type;
	uint16_t			flags;
	uint8_t				body[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));

struct of10_stats_reply {
	struct ofp_header		hdr;
	uint16_t			type;
	uint16_t			flags;
	uint8_t				body[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));

struct of10_flow_stats_request
{
	struct of10_match		match;
	uint8_t				table_id;
	uint8_t				pad;
	uint16_t			out_port;
}__attribute__((packed));

struct of10_flow_stats
{
	uint16_t			length;
	uint8_t				table_id;
	uint8_t				pad;
	struct of10_match		match;
	uint32_t			duration_sec;
	uint32_t			duration_nsec;
	uint16_t			priority;
	uint16_t			idle_timeout;
	uint16_t			hard_timeout;
	uint8_t				pad2[6];
	uint64_t			cookie;
	uint64_t			packet_count;
	uint64_t			byte_count;
	struct of10_action_header	actions[0];	/* XXX: won't be considered by sizeof */
}__attribute__((packed));

struct of10_port_stats_request
{
	uint16_t			port_no;
	uint8_t				pad[6];
}__attribute__((packed));

struct of10_port_stats
{
	uint16_t			port_no;
	uint8_t				pad[6];
	uint64_t			rx_packets;
	uint64_t			tx_packets;
	uint64_t			rx_bytes;
	uint64_t			tx_bytes;
	uint64_t			rx_dropped;
	uint64_t			tx_dropped;
	uint64_t			rx_errors;
	uint64_t			tx_errors;
	uint64_t			rx_frame_err;
	uint64_t			rx_over_err;
	uint64_t			rx_crc_err;
	uint64_t			collisions;
}__attribute__((packed));

const struct of_protocol *of10_protocol();

#endif
